const reducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return { data: action.payload };
    case "ADD":
      return { data: [...state.data, action.payload] };
    case "DELETE":
      const updatedState = state.data.filter((item) => item.id !== action.payload);
      return { data: [...updatedState] };
    case "UPDATE_STATE":
      const updatedItems = state.data.map((item) =>
        item.id === action.payload.checkItemId
          ? { ...item, state: action.payload.newState }
          : item
      );
      return { data: updatedItems };
    default:
      return state;
  }
};

export default reducer;
