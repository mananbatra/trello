import React from "react";
import Paper from '@mui/material/Paper';
import { CardContent } from "@mui/material";
import Typography from "@mui/material/Typography";


const Boards = ({ image, name}) => {
  const styles = {
    paper: {
      minWidth: 200,
      minHeight: 180,
      color: 'white',
      backgroundColor: '#3F4D52',
      backgroundImage: image? `url(${image})`:`url(../../../src/assets/img-1.jpg)`,
      backgroundSize: 'cover',
      margin: '2%',
    },
    title: {
      fontWeight: 'bold',
    },
  }

  return (
    <Paper elevation={10} style={styles.paper}>
      
      <CardContent>
        <Typography variant="h6" style={styles.title}>
          {name}
        </Typography>
      </CardContent>
    </Paper>
  );
};

export default Boards;
