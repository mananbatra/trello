import React, { useEffect, useState } from "react";
import Boards from "./Boards/Boards";
import { Box } from "@mui/material";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import Toast from "../Toast/Toast";
import { addBoard, getBoards } from "../api";
import Loader from "../Loader/Loader";
import AddItemBox from "../AddItemContainer/AddItemContainer";
import { useDispatch, useSelector } from "react-redux";
import { setBoards, addBoards, showToast, hideToast } from "../Redux/Slices/boardSlice";

const BoardContainer = () => {
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const boards = useSelector((state) => state.board.boards);
  const toastOpen = useSelector((state) => state.board.toastOpen);
  const toastSeverity = useSelector((state) => state.board.toastSeverity);
  const toastMessage = useSelector((state) => state.board.toastMessage);

  useEffect(() => {
    getBoards()
      .then((boards) => {
        dispatch(setBoards({ data: boards }));
      })
      .catch((err) => {
        dispatch(
          showToast({
            message: "Boards were unable to fetch! Try Again",
            severity: "error",
          })
        );
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  const handleAddBoard = (name) => {
    addBoard(name)
      .then((response) => {
        dispatch(addBoards(response));
        dispatch(
          showToast({
            message: `Board was added successfully with name ${name}`,
            severity: "success",
          })
        );
      })
      .catch((err) => {
        dispatch(
          showToast({
            message: "Board was unable to create",
            severity: "error",
          })
        );
      });
  };

  const hideToastHandler = () => {
    dispatch(hideToast());
  };

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <Container
          style={{
            backgroundColor: "#a7adaf",
            padding: "10px",
            minHeight: "87vh",
            backgroundImage: "url(../../src/assets/img-4.jpg)",
          }}
        >
          <Box display="flex" alignItems="center" flexWrap="wrap" gap="3rem">
            {boards? (
              boards.map((board) => (
                <Link to={`/boards/${board.id}`} key={board.id}>
                  <Boards
                    image={board.prefs.backgroundImage}
                    name={board.name}
                    color={board.prefs.backgroundColor}
                  />
                </Link>
              ))
            ) : (
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  opacity: ".5",
                  marginTop: "10%",
                }}
              >
                <h1 style={{ color: "#900000" }}>Create A Board</h1>
              </div>
            )}
            {boards.length === 10 ? null : (
              <AddItemBox
                minWidth="160px"
                minHeight="150px"
                placeholder="New Board"
                buttonText="Add New Board"
                onAdd={handleAddBoard}
                marginTop="0%"
              />
            )}
          </Box>
          <Toast
            open={toastOpen}
            onClose={hideToastHandler}
            message={toastMessage}
            severity={toastSeverity}
          />
        </Container>
      )}
    </>
  );
};

export default BoardContainer;
