import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import Button from '@mui/material/Button';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton } from "@mui/material";

const Delete = ({handleDelete}) => {

    const [open, setOpen] = useState(false);

  const handleClickOpen = (event) => {
    setOpen(!open);
  };

  const handleClose = () => {
    setOpen(!open);
  };

  const handleConfirmDelete = () => {
    handleDelete(); // Call the handleDelete function passed as a prop
    setOpen(!open); // Close the dialog after deletion
  };

  return (
    <>
        <div>
        <IconButton
        aria-label="delete"
        color="error"
        size="small"
        onClick={handleClickOpen}
      >
        <DeleteIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Delete Item"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This action will delete your selected Item and its components permanently and can't be undone later
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <IconButton
        aria-label="delete"
        color="error"
        size="small"
        onClick={handleConfirmDelete}
      >
        <DeleteIcon />
      </IconButton>
          <Button onClick={handleClose} autoFocus>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    </>
  );
};

export default Delete;
