import React from "react";
import { Box, Typography} from "@mui/material";
import Delete from "../../DeleteItem/Delete";
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckItemContainer from "../../CheckItemsContainer/CheckItemsContainer";

const Checklist = ({ checklist, handleDelete }) => {

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="flex-start"
      marginBottom="10px"
    >
      <Box display="flex" alignItems="center" width="100%">
        <CheckBoxIcon />
        <Typography variant="h5" style={{ marginRight: "10px", width: "80%" }}>
          {checklist.name}
        </Typography>
          <Delete handleDelete={handleDelete} />
      </Box>
      <CheckItemContainer checklist={checklist}/>
    </Box>
  );
};

export default Checklist;
