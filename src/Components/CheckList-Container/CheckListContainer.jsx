import React, {  useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { addCheckLists, deleteCheckList, getCheckLists } from "../api";
import { TextField } from "@mui/material";
import Checklist from "./CheckList/CheckList";
import Toast from "../Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { setCheckList, addCheckList as addAction , deleteCheckList as deleteAction} from "../Redux/Slices/checkListSlice";

const CheckListContainer = ({ card }) => {

  const dispatch = useDispatch();
  const checkLists = useSelector((state) => state.checklist.checkList);

  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [toastOpen, setToastOpen] = useState(false);
  const [toastSeverity, setToastSeverity] = useState("success");
  const [toastMessage, setToastMessage] = useState("");

  const handleClickOpen = () => {
    fetchCheckList();
  };

  const handleClose = () => {
    setOpen(false);
  };

  function fetchCheckList() {
    getCheckLists(card.id)
      .then((checkList) => {
        dispatch(setCheckList(checkList));
        setOpen(true);
      })
      .catch((err) => {
        showToast(`CheckLists were unable to fetch ! Try Again`, "error");
      });
  }

  const handleAddCheckList = (name) => {
    addCheckLists(name, card.id)
      .then((response) => {
        dispatch(addAction(response));
        showToast(`CheckLists was created with name ${name}`, "success");
      })
      .catch((err) => {
        showToast(`CheckList was unable to create ! Try Again`, "error");
      });
  };

  const handleDeleteCheckList = (checkListId) => {
    deleteCheckList(card.id, checkListId)
      .then((response) => {
        dispatch(deleteAction(checkListId));
        showToast(`CheckList was deleted`, "success");
      })
      .catch((err) => {
        showToast(`CheckList was unable to delete ! Try Again`, "error");
      });
  };

  const showToast = (message, severity) => {
    setToastMessage(message);
    setToastSeverity(severity);
    setToastOpen(true);
  };

  return (
    <div>
      <Button onClick={handleClickOpen} sx={{ maxWidth: "160px" }}>
        {card.name}
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        scroll="paper"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle
          id="scroll-dialog-title"
          sx={{
            justifyContent: "space-between",
            display: "flex",
            fontSize: "30px",
          }}
        >
          {card.name}
          <Button onClick={handleClose}>X</Button>
        </DialogTitle>
        <DialogContent dividers={scroll === "paper"}>
          <DialogContent>
            <TextField
              size="small"
              placeholder="New CheckList"
              color="primary"
              focused
              value={name}
              onChange={(event) => {
                setName(event.target.value);
              }}
              variant="outlined"
              sx={{ color: "white" }}
            />
            <Button
              variant="outline-secondary"
              id="button-addon2"
              onClick={(event) => {
                if (name) {
                  handleAddCheckList(name);
                  setName("");
                }
              }}
            >
              Add New CheckList
            </Button>
          </DialogContent>
          <DialogContent id="scroll-dialog-description" tabIndex={-1}>
            {checkLists.map((checklist) => {
              return (
                <Checklist
                  checklist={checklist}
                  key={checklist.id}
                  handleDelete={() => handleDeleteCheckList(checklist.id)}
                />
              );
            })}
          </DialogContent>
        </DialogContent>
      </Dialog>
      <Toast
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        message={toastMessage}
        severity={toastSeverity}
      />
    </div>
  );
};

export default CheckListContainer;
