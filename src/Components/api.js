import axios from "axios";

const API_KEY = "f4decfaf69dfb0bb55f9dff85fca244a";
const TOKEN = "ATTA23f9f7a1d4d14450b7fd9b5245922d390c67a781170ce67cdb8d70a8b2f7b17fA56E4ECC";
const BASE_URL = "https://api.trello.com/1/";

export const getBoards = () => {
  const boardsUrl = `${BASE_URL}members/me/boards?key=${API_KEY}&token=${TOKEN}`;
  return axios.get(boardsUrl).then((response) => response.data);
};

export const addBoard = (name) => {
  const addBoardUrl = `${BASE_URL}boards/?name=${name}&key=${API_KEY}&token=${TOKEN}`;
  return axios.post(addBoardUrl).then((response) => response.data);
};

export const getLists = (boardId) =>{

    const listsUrl= `${BASE_URL}boards/${boardId}/lists?key=${API_KEY}&token=${TOKEN}`;
    return axios.get(listsUrl).then((response)=> response.data)
}

export const addList = (name,id) => {
    const addListUrl = `${BASE_URL}boards/${id}/lists?name=${name}&key=${API_KEY}&token=${TOKEN}`;
    return axios.post(addListUrl).then((response) => response.data);
  };

export const getCards = (listId) =>{

    const cardsUrl= `${BASE_URL}lists/${listId}/cards?key=${API_KEY}&token=${TOKEN}`;
    return axios.get(cardsUrl).then((response)=> response.data)
}


export const addCard= (name,listId) =>{

    const addCardUrl = `${BASE_URL}cards?name=${name}&idList=${listId}&key=${API_KEY}&token=${TOKEN}`;
    // console.log(addCardUrl);
    return axios.post(addCardUrl).then((response)=>response.data);
}

export const deleteList = (id) => {
  const deleteListUrl = `${BASE_URL}lists/${id}?closed=true&key=${API_KEY}&token=${TOKEN}`;
  return axios.put(deleteListUrl).then((response) => response.data);
};

export const deleteCard = (id) => {
  const deleteCardUrl = `${BASE_URL}cards/${id}?key=${API_KEY}&token=${TOKEN}`;
  return axios.delete(deleteCardUrl).then((response) => response.data);
};

export const addCheckLists = (name,cardId) =>{

  const checkListsUrl= `${BASE_URL}checklists?name=${name}&idCard=${cardId}&key=${API_KEY}&token=${TOKEN}`;
  return axios.post(checkListsUrl).then((response)=> response.data)
}

export const getCheckLists = (cardId) =>{

  const checkListUrl= `${BASE_URL}cards/${cardId}/checklists?key=${API_KEY}&token=${TOKEN}`;
  return axios.get(checkListUrl).then((response)=> response.data)
}

export const deleteCheckList = (CardId,CheckListId) => {
  const deleteCheckListUrl = `${BASE_URL}cards/${CardId}/checklists/${CheckListId}?key=${API_KEY}&token=${TOKEN}`;
  return axios.delete(deleteCheckListUrl).then((response) => response.data);
};

export const addCheckItems = (name,checkListId) =>{

  const addCheckItemUrl= `${BASE_URL}checklists/${checkListId}/checkItems?name=${name}&key=${API_KEY}&token=${TOKEN}`;
  return axios.post(addCheckItemUrl).then((response)=> response.data)
}

export const getCheckItems = (checkListId) =>{

  const checkItemUrl= `${BASE_URL}checklists/${checkListId}/checkItems?key=${API_KEY}&token=${TOKEN}`;
  return axios.get(checkItemUrl).then((response)=> response.data)
}

export const deleteCheckItems = (checkListId, checkItemId) => {
  const deleteCheckItemUrl = `${BASE_URL}checklists/${checkListId}/checkItems/${checkItemId}?key=${API_KEY}&token=${TOKEN}`;
  return axios.delete(deleteCheckItemUrl).then((response) => response.data);
};

export const updateCheckItem = (idCard, idCheckItem, status) => {
  const updateCheckItemUrl = `${BASE_URL}cards/${idCard}/checkItem/${idCheckItem}?key=${API_KEY}&token=${TOKEN}`;
  const data = {
    state: status
  };
  return axios.put(updateCheckItemUrl, data).then((response) => response.data);
};
