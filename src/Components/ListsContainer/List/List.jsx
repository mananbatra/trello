import { CardContent, IconButton, Paper, Typography } from '@mui/material';
import React from 'react'
import CardContainer from '../../CardContainer/CardContainer';
import DeleteIcon from "@mui/icons-material/Delete";
import Delete from '../../DeleteItem/Delete';

const List = ({name,id,handleDeleteList}) => {
    const styles = {
        paper: {
          minWidth: '240px',
          minHeight: 50,
          color: 'white',
          backgroundColor: '#3F4D52',
          margin: '2%',
        },
        title: {
          fontWeight: 'bold',
          display:'flex',
          justifyContent:'space-between'
        },
      }
    
      return (
        <Paper elevation={10} style={styles.paper}>
          
          <CardContent>
            <Typography variant="h6" style={styles.title} >
              {name}
              <Delete handleDelete={handleDeleteList}/>
            </Typography>
            <CardContainer listId={id}/>
          </CardContent>
        </Paper>
      );
    };


export default List