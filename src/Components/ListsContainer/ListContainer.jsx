import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { Box } from '@mui/material';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getLists ,addList,deleteList} from '../api';
import List from './List/List';
import AddItemBox from '../AddItemContainer/AddItemContainer';
import Toast from '../Toast/Toast';
import { setLists, addList as addListAction, deleteList as deleteListAction, showToast, hideToast } from '../Redux/Slices/listSlice';

const ListContainer = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const lists = useSelector((state) => state.list.lists);
  const toastOpen = useSelector((state) => state.list.toastOpen);
  const toastSeverity = useSelector((state) => state.list.toastSeverity);
  const toastMessage = useSelector((state) => state.list.toastMessage);

  useEffect(() => {
    getLists(id)
      .then((lists) => {
        dispatch(setLists({ data: lists }));
      })
      .catch(() => {
        dispatch(
          showToast({
            message: 'Lists were unable to fetch! Try Again',
            severity: 'error',
          })
        );
      });
  }, []);


  const handleAddList = (name) => {
    addList(name, id)
      .then((response) => {
        dispatch(addListAction(response));
        dispatch(
          showToast({
            message: 'CheckList was added successfully',
            severity: 'success',
          })
        );
      })
      .catch(() => {
        dispatch(
          showToast({
            message: 'CheckList was unable to add! Try Again',
            severity: 'error',
          })
        );
      });
  };

  const handleDeleteList = (listId) => {
    deleteList(listId)
      .then(() => {
        dispatch(deleteListAction(listId));
        dispatch(
          showToast({
            message: 'CheckList was deleted successfully',
            severity: 'success',
          })
        );
      })
      .catch(() => {
        dispatch(
          showToast({
            message: 'CheckList was unable to delete! Try Again',
            severity: 'error',
          })
        );
      });
  };

  const hideToastHandler = () => {
    dispatch(hideToast());
  };

  return (
    <Container
      style={{
        backgroundColor: '#a7adaf',
        padding: '10px',
        height: '87vh',
        backgroundImage: 'url(../../../src/assets/img-4.jpg)',
      }}
    >
      <div style={{ overflowX: 'auto', height: '100%' }}>
        <Box display="flex" alignItems="start">
          {lists
            ? lists.map((list) => (
                <List
                  key={list.id}
                  name={list.name}
                  id={list.id}
                  handleDeleteList={() => handleDeleteList(list.id)}
                />
              ))
            : null}
          <AddItemBox
            minWidth="150px"
            minHeight="100px"
            marginTop="2%"
            placeholder="NewList"
            buttonText="Add New List"
            onAdd={handleAddList}
          />
        </Box>
      </div>
      <Toast
        open={toastOpen}
        onClose={hideToastHandler}
        message={toastMessage}
        severity={toastSeverity}
      />
    </Container>
  );
};

export default ListContainer;
