import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar sx={{justifyContent:"space-between"}}>
          <Link to='/'>
            <Button color="inherit">
              <i className="fa-solid fa-clapperboard" style={{color: "#e5e9f0"}}></i>
              Boards
            </Button>
          </Link>
          <Typography variant="h6" component="div">
            <i
              className="fa-brands fa-trello fa-xl"
              style={{ color: "#5c98ff" }}
            ></i>
            Trello
          </Typography>
          <Button color="inherit">
            <i
              className={`fa-regular fa-moon`}
              style={{ color: "#ffffff" }}
            ></i>
            Dark Mode
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
