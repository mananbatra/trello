import React from 'react'
import './ErrorPage.css'

const NotFoundPage = () => {
  return (
    <div className="error-container">
    <div className="error-content">
      <h1>Oops! Error 404.</h1>
      <p>We apologize for the inconvenience. The page you are looking for does not exist.</p>
      <a href="/" className="button">Go back to homepage</a>
    </div>
  </div>
);
}

export default NotFoundPage