import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";

const CreateCard = ({handleAddCard}) => {

  const [displayAddCardField, setDisplayAddCardField] = useState(false);
  const [name, setName] = useState(undefined);



  return (
    <Card sx={{ maxWidth: "200px" }}>
      {!displayAddCardField && (
        <Button
        sx={{ width: "100%" }}
          onClick={() => {
            setDisplayAddCardField(true);
          }}
        >
          + Add Card
        </Button>
      )}
      {displayAddCardField && (
          <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
            <TextField
              sx={{ width: "100%" }}
              required
              id="outlined-required"
              label="Card Title"
              variant="outlined"
              onChange={(event) => {
                setName(event.target.value);
              }}
            />
            <ButtonGroup aria-label=" secondary button group" sx={{ display: "flex" , justifyContent:'space-between'}} >
              <Button
                type="submit"
                size="large"
                variant="contained"
                onClick={() => {
                    if (name) {
                      handleAddCard(name);
                      setDisplayAddCardField(false);
                    }
                  }}
              >
                Add Card
              </Button>
              <Button
                size="small"
                variant="text"
                onClick={() => {
                  setDisplayAddCardField(false);
                }}
              >
                X
              </Button>
            </ButtonGroup>
          </Stack>
      )}
    </Card>
  );
};

export default CreateCard;