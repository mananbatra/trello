import React, { useEffect, useState } from "react";
import { addCard, deleteCard, getCards } from "../api";
import CreateCard from "./CreateCard/CreateCard";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import { styled } from "@mui/material/styles";
import Delete from "../DeleteItem/Delete";
import CheckListContainer from "../CheckList-Container/CheckListContainer";
import Toast from "../Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import {setCards, addCard as addCardAction, deleteCard as deleteCardAction} from "../Redux/Slices/cardSlice"; 

const CardContainer = ({ listId }) => {

  const dispatch = useDispatch();
  const cards = useSelector((state) => state.card.card[listId] || []);

  const [toastOpen, setToastOpen] = useState(false);
  const [toastSeverity, setToastSeverity] = useState("success");
  const [toastMessage, setToastMessage] = useState("");

  useEffect(() => {

    getCards(listId)
      .then((cards) => {
        dispatch(setCards({ id: listId, data: cards }));
      })
      .catch((err) => {
        showToast(`Cards were unable to fetch! Try Again`, "error");
      });
  }, []);

  const handleAddCard = (name) => {
    addCard(name, listId)
      .then((response) => {
        dispatch(addCardAction({ id: listId, data: response }));
        showToast(`Card was created successfully with name ${name}`, "success");
      })
      .catch((err) => {
        showToast(`Card was unable to create ! Try Again`, "error");
      });
  };

  const handleDeleteCard = (cardId) => {
    deleteCard(cardId)
      .then((response) => {
        dispatch(deleteCardAction({listId, cardId}));
        showToast(`Card was deleted successfully`, "success");
      })
      .catch((err) => {
        showToast(`Card was not able to be deleted! Try Again`, "error");
      });
  };

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  }));

  const showToast = (message, severity) => {
    setToastMessage(message);
    setToastSeverity(severity);
    setToastOpen(true);
  };

  return (
    <>
      <Stack spacing={2} width={"200px"}>
      {cards&&cards
      .map(card=>{
          return (
            <Item
              key={card.id}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <CheckListContainer card={card} />
              <Delete handleDelete={() => handleDeleteCard(card.id)} />
            </Item>
          );
        })}
      </Stack>
      <br />
      <CreateCard handleAddCard={handleAddCard} />
      <Toast
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        message={toastMessage}
        severity={toastSeverity}
      />
    </>
  );
};

export default CardContainer;
