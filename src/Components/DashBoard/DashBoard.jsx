import React from "react";
import TitleBar from "../TitleBar/TitleBar";
import BoardContainer from "../BoardContainer/BoardContainer";
import { Route, Routes } from "react-router-dom";
import ListContainer from "../ListsContainer/ListContainer";
import NotFoundPage from "../ErrorPage/NotFoundPage";

const DashBoard = () => {
  return (
    <>
      <TitleBar />
      <Routes>
        <Route
          path="/"
          element={
            <BoardContainer/>
          }
        />
        <Route path="boards/:id" element={<ListContainer/>} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </>
  );
};

export default DashBoard;
