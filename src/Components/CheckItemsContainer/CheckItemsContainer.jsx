import React, { useEffect, useReducer, useState } from "react";
import Button from "@mui/material/Button";
import {
  addCheckItems,
  deleteCheckItems,
  getCheckItems,
  updateCheckItem,
} from "../api";
// import ErrorPage from "../ErrorPage/ErrorPage";
import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  LinearProgress,
  TextField,
  Typography,
} from "@mui/material";
import Delete from "../DeleteItem/Delete";
import Toast from "../Toast/Toast";
import { useDispatch, useSelector } from "react-redux";
import { setCheckItems,addCheckItem, deleteCheckItem, updateCheckItemState} from "../Redux/Slices/checkItemSlice";


const CheckItemContainer = ({ checklist }) => {
  
  const dispatch = useDispatch();
  const checkItems = useSelector((state) => state.checkItem[checklist.id]||[]);
  console.log(checkItems);
  const [name, setName] = useState("");
  const [toastOpen, setToastOpen] = useState(false);
  const [toastSeverity, setToastSeverity] = useState("success");
  const [toastMessage, setToastMessage] = useState("");

  useEffect(() => {
    getCheckItems(checklist.id)
      .then((checkItems) => {
        dispatch(setCheckItems({ checklistId: checklist.id, checkItems }));
      })
      .catch((err) => {
        showToast(`CheckItem's were unable to fetch`, "error");
      });
  }, [checklist.id]);

  const handleAddCheckItem = (name) => {
    addCheckItems(name, checklist.id)
      .then((response) => {
        dispatch(addCheckItem({ checklistId: checklist.id, checkItem: response }));
        // console.log(response);
        showToast(`CheckItem was created with name ${name}`, "success");
      })
      .catch((err) => {
        showToast(
          `CheckItem was unable to be created with name ${name}`,
          "error"
        );
      });
  };

  const handleDeleteCheckItem = (checkItemId) => {
    deleteCheckItems(checklist.id, checkItemId)
      .then((response) => {
        dispatch(deleteCheckItem({ checklistId: checklist.id, checkItemId: checkItemId }));
        showToast(`CheckItem deleted`, "success");
      })
      .catch((err) => {
        setError(err);
        showToast(`CheckItem was unable to delete`, "error");
      });
  };

  const handleUpdateState = (checkItemId, state) => {
    const newState = state === "incomplete" ? "complete" : "incomplete";

    updateCheckItem(checklist.idCard, checkItemId, newState)
      .then((response) => {
        dispatch(updateCheckItemState({
          checklistId: checklist.id,
          checkItemId,
          newState,
        }));
      })
      .catch((error) => {
        showToast(`Unable to check! Try Again`, "error");
      });
  };

  const showToast = (message, severity) => {
    setToastMessage(message);
    setToastSeverity(severity);
    setToastOpen(true);
  };

  let completionPercentage = 0;
  if (checkItems.length !== 0) {
    const completedItems = checkItems.filter(
      (item) => item.state === "complete"
    );
    completionPercentage = (completedItems.length / checkItems.length) * 100;
  }

  // if (error) {
  //   return <ErrorPage />;
  // }

  return (
    <div>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ width: "100%", mr: 1 }}>
          <LinearProgress variant="determinate" value={completionPercentage} />
        </Box>
        <Box sx={{ minWidth: 35 }}>
          <Typography variant="body2" color="text.secondary">{`${Math.round(
            completionPercentage
          )}%`}</Typography>
        </Box>
      </Box>
      <FormGroup>
        {checkItems.map((checkitem) => {
          return (
            <div key={checkitem.id} style={{ display: "flex" }}>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkitem.state === "complete"}
                    onChange={() =>
                      handleUpdateState(checkitem.id, checkitem.state)
                    }
                  />
                }
                label={checkitem.name}
              />
              <Delete
                handleDelete={() => handleDeleteCheckItem(checkitem.id)}
              />
            </div>
          );
        })}
      </FormGroup>
      <br />
      <TextField
        size="small"
        placeholder="Item"
        color="primary"
        focused
        value={name}
        onChange={(event) => {
          setName(event.target.value);
        }}
        variant="outlined"
        sx={{ color: "white" }}
      />
      <Button
        variant="outline-secondary"
        id="button-addon2"
        onClick={(event) => {
          if (name) {
            handleAddCheckItem(name);
            setName("");
          }
        }}
      >
        Add
      </Button>
      <Toast
        open={toastOpen}
        onClose={() => setToastOpen(false)}
        message={toastMessage}
        severity={toastSeverity}
      />
    </div>
  );
};

export default CheckItemContainer;
