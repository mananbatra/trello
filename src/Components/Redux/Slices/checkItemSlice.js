import { createSlice } from "@reduxjs/toolkit";

const initialCheckItemState = {};

const checkItemSlice = createSlice({
  name: "checkItemSlice",
  initialState: initialCheckItemState,
  reducers: {
    addCheckItem(state, action) {
      const { checklistId, checkItem } = action.payload;
      if (!state[checklistId]) {
        state[checklistId] = [];
      }
      state[checklistId].push(checkItem);
    },
    setCheckItems(state, action) {
      const { checklistId, checkItems } = action.payload;
      state[checklistId] = checkItems;
    },
    deleteCheckItem(state, action) {
      const { checklistId, checkItemId } = action.payload;
      if (state[checklistId]) {
        state[checklistId] = state[checklistId].filter(
          (checkItem) => checkItem.id !== checkItemId
        );
      }
    },
    updateCheckItemState(state, action) {
      const { checklistId, checkItemId, newState } = action.payload;
      if (state[checklistId]) {
        const updatedCheckItems = state[checklistId].map((item) =>
          item.id === checkItemId ? { ...item, state: newState } : item
        );
        state[checklistId] = updatedCheckItems;
      }
    },
  },
});

export const {
  setCheckItems,
  addCheckItem,
  deleteCheckItem,
  updateCheckItemState,
} = checkItemSlice.actions;

export default checkItemSlice.reducer;
