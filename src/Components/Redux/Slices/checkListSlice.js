import { createSlice } from "@reduxjs/toolkit";

const checkListSlice = createSlice({
    name:'checkListSlice',
    initialState:{
        checkList:[],
    },
    reducers:{
        setCheckList(state,action){
            state.checkList=[...action.payload];
        },
        addCheckList(state, action){
            state.checkList.push(action.payload);
        },
        deleteCheckList(state,action){
            let newData = state.checkList.filter((checklist)=>{
                return checklist.id!= action.payload;
            });
            state.checkList = newData;
        },
        
    }
});


export const {setCheckList, addCheckList, deleteCheckList} = checkListSlice.actions;
export default checkListSlice.reducer;