import { createSlice } from "@reduxjs/toolkit";

const initialCardState = {
  card: {},
};

const cardSlice = createSlice({
  name: "cardSlice",
  initialState: initialCardState,
  reducers: {
    addCard(state, action) {
      const { id, data } = action.payload;
      if (!state.card[id]) {
        state.card[id] = [];
      }
      state.card[id].push(data);
    },
    setCards(state, action) {
      const { id, data } = action.payload;
    //   console.log(id,"hello",data)
      state.card[id] = data;
    },
    deleteCard(state, action) {
      const { listId, cardId } = action.payload;
      state.card[listId] = state.card[listId].filter((card) => card.id !== cardId);
    },
  },
});

export const { setCards, addCard, deleteCard} = cardSlice.actions;
export default cardSlice.reducer;
