import { createSlice } from '@reduxjs/toolkit';

const listSlice = createSlice({
  name: 'list',
  initialState: {
    lists: [],
    toastOpen: false,
    toastSeverity: 'success',
    toastMessage: '',
  },
  reducers: {
    setLists: (state, action) => {
      state.lists = action.payload.data;
    },
    addList: (state, action) => {
      state.lists.push(action.payload);
    },
    deleteList: (state, action) => {
      state.lists = state.lists.filter(list => list.id !== action.payload);
    },
    showToast: (state, action) => {
      state.toastOpen = true;
      state.toastSeverity = action.payload.severity;
      state.toastMessage = action.payload.message;
    },
    hideToast: (state) => {
      state.toastOpen = false;
    },
  },
});

export const { setLists, addList, deleteList, showToast, hideToast } = listSlice.actions;
export default listSlice.reducer;