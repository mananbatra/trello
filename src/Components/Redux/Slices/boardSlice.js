import { createSlice } from "@reduxjs/toolkit";

const boardsSlice = createSlice({
  name: "boards",
  initialState: {
    boards: [],
    isLoadingBoards: true,
    error: undefined,
    toastOpen: false,
    toastSeverity: "success",
    toastMessage: "",
  },
  reducers: {
    setBoards: (state, action) => {
      state.boards = action.payload.data;
      state.isLoadingBoards = false;
      state.error = action.payload.error;
    },
    addBoards: (state, action) => {
      state.boards.push(action.payload);
    },
    showToast: (state, action) => {
      state.toastOpen = true;
      state.toastSeverity = action.payload.severity;
      state.toastMessage = action.payload.message;
    },
    hideToast: (state) => {
      state.toastOpen = false;
    },
  },
});

export const { setBoards, addBoards, showToast, hideToast } =
  boardsSlice.actions;
export default boardsSlice.reducer;
