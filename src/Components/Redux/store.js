import { configureStore } from "@reduxjs/toolkit";
import boardReducer from "./Slices/boardSlice";
import listReducer from "./Slices/listSlice";
import cardSlice from "./Slices/cardSlice";
import checkListSlice from "./Slices/checkListSlice";
import checkItemSlice from "./Slices/checkItemSlice";

export const store = configureStore({
  reducer: {
    board: boardReducer,
    list: listReducer,
    card: cardSlice,
    checklist: checkListSlice,
    checkItem:checkItemSlice
  },
});
