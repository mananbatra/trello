import React, { useState } from "react";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

function AddItemBox({
  minWidth,
  minHeight,
  marginTop,
  onAdd,
  placeholder,
  buttonText,
}) {
  const [name, setName] = useState("");

  const handleAdd = () => {
    if (name) {
      onAdd(name);
      setName("");
    }
  };

  return (
    <Paper
      elevation={3}
      style={{
        minWidth:minWidth,
        minHeight:minHeight,
        width: minWidth,
        height: minHeight,
        backgroundColor: "#3F4D52",
        padding: "1%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        color: "white",
        marginLeft: "1%",
        marginTop: marginTop,
      }}
    >
      <TextField
        size="small"
        placeholder={placeholder}
        color="primary"
        focused
        value={name}
        onChange={(event) => {
          setName(event.target.value);
        }}
        variant="outlined"
        sx={{ color: "white", background: "blue" }}
      />
      <br />
      <Button
        variant="outline-secondary"
        id="button-addon2"
        onClick={handleAdd}
      >
        {buttonText}
      </Button>
    </Paper>
  );
}

export default AddItemBox;
