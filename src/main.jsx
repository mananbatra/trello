import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { DarkThemeContextProvider } from "./DarkThemeContext.jsx";
import { store } from "../src/Components/Redux/store";
import { Provider } from "react-redux";

ReactDOM.createRoot(document.getElementById("roots")).render(
  // <React.StrictMode>
  <BrowserRouter>
    <Provider store={store}>
      <DarkThemeContextProvider>
        <App />
      </DarkThemeContextProvider>
    </Provider>
  </BrowserRouter>
  // </React.StrictMode>,
);
